package my.test.org.db.dao;

import my.test.org.web.datagrid.SortDirection;
import my.test.org.db.model.DictionaryNameModel;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * Created by Evgeny on 28.05.2015.
 */
public interface DictionaryNameDAO {
    void insert(@Param("model") DictionaryNameModel model);
    List<DictionaryNameModel> getList();
    Long getCount(@Param("filter") Map<String, Object> filter);
    List<DictionaryNameModel> getListOnPage(@Param("filter") Map<String, Object> filter,
                                            @Param("sortField") String sortField,
                                            @Param("sortDirection") SortDirection sortDirection,
                                            @Param("rowCount") Long rowCount,
                                            @Param("offset") Long offset);
}

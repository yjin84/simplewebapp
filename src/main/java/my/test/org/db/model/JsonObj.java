package my.test.org.db.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import my.test.org.web.datagrid.SortDirection;

/**
 * Created by Evgeny on 30.05.2015.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class JsonObj {
    private String name;
    private SortDirection sortDirection;

    @Override
    public String toString() {
        return new StringBuilder()
                .append("name = ").append(name)
                .append(", sortDirection = ").append(sortDirection)
                .toString();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SortDirection getSortDirection() {
        return sortDirection;
    }

    public void setSortDirection(SortDirection sortDirection) {
        this.sortDirection = sortDirection;
    }
}

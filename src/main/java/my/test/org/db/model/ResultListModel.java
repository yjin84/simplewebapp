package my.test.org.db.model;

import java.util.List;

/**
 * Created by Evgeny on 30.05.2015.
 */
public class ResultListModel<T> {
    protected List<T> rows;
    protected Long pageSize;
    protected Long total;

    @Override
    public String toString() {
        return new StringBuilder()
                .append("rows = ").append(rows)
                .append(", pageSize = ").append(pageSize)
                .append(", total = ").append(total)
                .toString();
    }

    public ResultListModel(Long totalRecords, Long pageNumder, Long pageSize) {
        this.pageSize = pageSize == null ? 1L : pageSize;
        this.total = totalRecords == null ? this.pageSize : totalRecords;
    }

    public List<T> getRows() {
        return rows;
    }

    public void setRows(List<T> rows) {
        this.rows = rows;
    }

    public Long getPageSize() {
        return pageSize;
    }

    public void setPageSize(Long pageSize) {
        this.pageSize = pageSize;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }
}

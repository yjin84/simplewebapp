package my.test.org.db.model;

/**
 * Модель записи справочника имен
 * Created by Evgeny on 28.05.2015.
 */
public class DictionaryNameModel {
    private Long id;        // идентификатор

    private String name;    // имя

    @Override
    public String toString() {
        return new StringBuilder()
                .append("id = ").append(id)
                .append(", name = ").append(name)
                .toString();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

package my.test.org.web.datagrid;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Класс для сущности datagrid
 * Created by Evgeny on 29.05.2015.
 */
public class DataGrid {
    private Long page = 1L;
    private Long rowCount = 10L;
    private Long offset = 0L;
    private String sortField;
    private SortDirection sortDirection = SortDirection.ASC;

    private Map<String, Object> filter = new HashMap<>();

    @Override
    public String toString() {
        return new StringBuilder()
                .append("sortField = ").append(sortField)
                .append(", sortDirection = ").append(sortDirection)
                .append(", page = ").append(page)
                .append(", rowCount = ").append(rowCount)
                .append(", offset = ").append(offset)
                .append(", filter = ").append(filter)
                .toString();
    }

    public static DataGrid valueOfRequestParams(Map<String, String> mapRequestParams, Filter filter) {
        DataGrid dataGrid = new DataGrid();

        if (mapRequestParams != null && !mapRequestParams.isEmpty() && filter != null) {
            Iterator<String> iterator = mapRequestParams.keySet().iterator();

            while (iterator.hasNext()) {
                String key = iterator.next();
                String value = null;
                if ("sort".equals(key)) {
                    value = mapRequestParams.get(key);
                    if (value != null && !value.isEmpty()) {
                        dataGrid.setSortField(value);
                    }
                }

                if ("order".equals(key)) {
                    value = mapRequestParams.get(key);
                    if (value != null && !value.isEmpty()) {
                        dataGrid.setSortDirection(SortDirection.valueOf(value.toUpperCase()));
                    }
                }

                if ("page".equals(key)) {
                    value = mapRequestParams.get(key);
                    if (value != null && !value.isEmpty()) {
                        dataGrid.setPage(Long.valueOf(value));
                    }
                }

                if ("rows".equals(key)) {
                    value = mapRequestParams.get(key);
                    if (value != null && !value.isEmpty()) {
                        dataGrid.setRowCount(Long.valueOf(value));
                    }
                }

                if (key.startsWith(Filter.FILTER_PREFIX)) {
                    String keyName = key.substring(key.indexOf(".") + 1);

                    if (filter.contains(keyName)) {
                        value = mapRequestParams.get(key);
                        if (value != null && !value.isEmpty()) {

                            switch (filter.getFilterPropertyByName(keyName).getType()) {
                                case STRING:
                                    dataGrid.addFilterParam(keyName,"%" + value + "%");
                                    break;
                                case INT:
                                    dataGrid.addFilterParam(keyName,Long.valueOf(value));
                                    break;
                                default:
                                    dataGrid.addFilterParam(keyName,Long.valueOf(value));
                                    break;
                            }
                        }
                    }
                }
            }
        }

        if (dataGrid.getPage().compareTo(1L) != 0) {
            dataGrid.setOffset(((dataGrid.getPage() - 1) * dataGrid.getRowCount()));
        }

        return dataGrid;
    }

    public Long getPage() {
        return page;
    }

    public void setPage(Long page) {
        this.page = page;
    }

    public Long getRowCount() {
        return rowCount;
    }

    public void setRowCount(Long rowCount) {
        this.rowCount = rowCount;
    }

    public Long getOffset() {
        return offset;
    }

    public void setOffset(Long offset) {
        this.offset = offset;
    }

    public String getSortField() {
        return sortField;
    }

    public void setSortField(String sortField) {
        this.sortField = sortField;
    }

    public SortDirection getSortDirection() {
        return sortDirection;
    }

    public void setSortDirection(SortDirection sortDirection) {
        this.sortDirection = sortDirection;
    }

    public Map<String, Object> getFilter() {
        return filter;
    }

    public void setFilter(Map<String, Object> filter) {
        this.filter = filter;
    }

    public void addFilterParam(String key, Object value) {
        if (filter == null) {
            filter = new HashMap<>();
        }

        if (key != null && !key.isEmpty()
                && value != null) {
            filter.put(key,value);
        }
    }
}

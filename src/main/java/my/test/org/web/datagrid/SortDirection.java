package my.test.org.web.datagrid;

/**
 * Created by Evgeny on 29.05.2015.
 */
public enum SortDirection {
    ASC,
    DESC;
}

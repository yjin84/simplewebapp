package my.test.org.web.datagrid;

import java.util.*;

/**
 * Класс для работы с фильтром
 * Created by Evgeny on 29.05.2015.
 */
public class Filter {
    public final static String FILTER_PREFIX = "filter";

    private Map<String, FilterProperty> mapFilterProperties = new HashMap<>();

    public Collection<FilterProperty> getFilterProperties() {
        return mapFilterProperties.values();
    }

    public FilterProperty getFilterPropertyByName(String filterName){
        if (mapFilterProperties.containsKey(filterName)) {
            return mapFilterProperties.get(filterName);
        }

        return null;
    }

    public void registerFilterProperty(FilterProperty filterProperty) {
        if (filterProperty != null) {
            mapFilterProperties.put(filterProperty.getName(), filterProperty);
        }
    }

    public boolean contains(String filterName) {
        return mapFilterProperties.containsKey(filterName);
    }
}

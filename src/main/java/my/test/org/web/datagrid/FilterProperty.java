package my.test.org.web.datagrid;

/**
 * Элемент фильтра
 * Created by Evgeny on 29.05.2015.
 */
public class FilterProperty {
    private String name;
    private FilterPropertyType type = FilterPropertyType.STRING;

    @Override
    public String toString() {
        return new StringBuilder()
                .append("name = ").append(name)
                .append(", type = ").append(type)
                .toString();
    }

    public FilterProperty(String name, FilterPropertyType type) {
        this.name = name;
        this.type = type;
    }

    public static FilterProperty valueOf(String name, FilterPropertyType type) {
        if (name != null && !name.isEmpty()
                && type != null) {
            return new FilterProperty(name, type);
        }

        return null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public FilterPropertyType getType() {
        return type;
    }

    public void setType(FilterPropertyType type) {
        this.type = type;
    }
}

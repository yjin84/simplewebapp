package my.test.org.web.services;

import my.test.org.web.datagrid.SortDirection;
import my.test.org.db.dao.DictionaryNameDAO;
import my.test.org.db.model.DictionaryNameModel;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * Сервис для работы со справочником имен
 * Created by Evgeny on 28.05.2015.
 */
@Service
public class DictionaryNameService implements DictionaryNameDAO {

    @Autowired
    private SqlSessionFactory sqlSessionFactory;


    @Override
    @Transactional
    public void insert(DictionaryNameModel model) {
        try (SqlSession session = sqlSessionFactory.openSession()) {
            DictionaryNameDAO dao = session.getMapper(DictionaryNameDAO.class);

            dao.insert(model);
        }
    }

    @Override
    public List<DictionaryNameModel> getList() {
        try (SqlSession session = sqlSessionFactory.openSession()) {
            DictionaryNameDAO dao = session.getMapper(DictionaryNameDAO.class);

            return dao.getList();
        }
    }

    @Override
    public Long getCount(@Param("filter") Map<String, Object> filter) {
        try (SqlSession session = sqlSessionFactory.openSession()) {
            DictionaryNameDAO dao = session.getMapper(DictionaryNameDAO.class);

            return dao.getCount(filter);
        }
    }

    @Override
    public List<DictionaryNameModel> getListOnPage(Map<String, Object> filter, String sortField, SortDirection sortDirection, Long rowCount, Long offset) {
        try (SqlSession session = sqlSessionFactory.openSession()) {
            DictionaryNameDAO dao = session.getMapper(DictionaryNameDAO.class);

            return dao.getListOnPage(filter, sortField, sortDirection, rowCount, offset);
        }
    }
}

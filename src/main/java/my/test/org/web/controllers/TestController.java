package my.test.org.web.controllers;

import my.test.org.db.model.DictionaryNameModel;
import my.test.org.db.model.JsonObj;
import my.test.org.db.model.ResultListModel;
import my.test.org.web.datagrid.DataGrid;
import my.test.org.web.datagrid.Filter;
import my.test.org.web.datagrid.FilterProperty;
import my.test.org.web.datagrid.FilterPropertyType;
import my.test.org.web.services.DictionaryNameService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.Arrays;
import java.util.Locale;

/**
 * Основной контроллер
 * Created by Evgeny on 28.05.2015.
 */
@Controller
public class TestController {
    private final static Logger log = LogManager.getLogger(TestController.class);

    @Autowired
    private DictionaryNameService service;

    @Autowired
    private MessageSource messageSource;

    @RequestMapping(value = "/")
    public ModelAndView main() {
        if (log.isDebugEnabled()) {
            log.debug("'/' - main page");
        }

        ModelAndView model = new ModelAndView("index");

        return model;
    }


    @RequestMapping(value = "/getList")
    public @ResponseBody Object getList(@RequestParam MultiValueMap mapQueryParams,
                                        Locale locale) {
        if (log.isDebugEnabled()) {
            log.debug("/getList");
        }
        try {
            Filter filter = new Filter();
            filter.registerFilterProperty(new FilterProperty("name", FilterPropertyType.STRING));
            DataGrid dataGrid = DataGrid.valueOfRequestParams(mapQueryParams.toSingleValueMap(),filter);



            ResultListModel resultListModel = new ResultListModel(service.getCount(dataGrid.getFilter()), dataGrid.getPage(), dataGrid.getRowCount());
            resultListModel.setRows(service.getListOnPage(dataGrid.getFilter(),
                    dataGrid.getSortField(),
                    dataGrid.getSortDirection(),
                    dataGrid.getRowCount(),
                    dataGrid.getOffset()));

            return resultListModel;
        } catch (Exception e) {
            log.error("Error during while get rows names", e);
            return messageSource.getMessage("getListError", Arrays.asList(e.getLocalizedMessage()).toArray(),locale);
        }
    }

    @RequestMapping(value = "/newRecord")
    public ModelAndView addRecord() {
        if (log.isDebugEnabled()) {
            log.debug("/newRecord");
        }

        ModelAndView model = new ModelAndView("add_name_form");

        return model;
    }

    @RequestMapping(value = "/saveRecord", produces = "text/html; charset=UTF-8")
    public @ResponseBody String saveRecord(DictionaryNameModel model, Locale locale) {
        if (log.isDebugEnabled()) {
            log.debug("/saveRecord");
        }

        try {
            service.insert(model);

            return messageSource.getMessage("success.add-new-record", null, locale);
        } catch (Exception e) {
            log.error("Error during while save record", e);
            return messageSource.getMessage("getListError", Arrays.asList(e.getLocalizedMessage()).toArray(), locale);
        }
    }

    @RequestMapping(value = "/sendJSON", consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody JsonObj getJSON(@RequestBody JsonObj obj) {
        System.out.println(obj);

        return obj;
    }
}

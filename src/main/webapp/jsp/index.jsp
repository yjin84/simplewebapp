<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="s" %>
<!DOCTYPE html>
<html>
    <head>
        <title>Тестовое приложение ORG</title>
        <link type="text/css" href="<c:url value="/resources/css/easyui/themes/default/easyui.css"/>" rel="stylesheet"/>
        <link type="text/css" href="<c:url value="/resources/css/easyui/themes/icon.css"/>" rel="stylesheet"/>
        <script type="text/javascript" src="<c:url value="/resources/js/jquery-2.1.4.min.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/resources/js/easyui/jquery.easyui.min.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/resources/js/easyui/locale/easyui-lang-ru.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/resources/js/extends.js"/>"></script>
        <script type="text/javascript">
            var conf = {
                url: "<c:url value="/getList" />",
                idField: 'id',
                sortName: 'name',
                sortOrder: 'asc',
                pagination: true,
                toolbar: '#tb',
                columns: [[
                    {field:'id',title:'<s:message code="dictName.id" />', align: "center", width: 50, sortable: true},
                    {field:'name',title:'<s:message code="dictName.name" />', align: "left", width: 250, sortable: true}
                ]],
                onLoadError: function(jqXHR) {
                    $.messager.alert('<s:message code="dialog.error" />',jqXHR.responseText,'error');
                }
            };

            var filter = [];

            function doFilter() {
                filter = [];
                $(".filter-field").each(function(){
                    if($.trim($(this).val()).length) {
                        filter['filter.' + $(this).attr("id")] = $.trim($(this).val());
                    }
                });

                $('#dg').datagrid('load',filter);
            }

            function initFilter() {
                $(".filter-field").each(function(){
                    $(this).on("keyup", function(){
                        var keycode = (event.keyCode ? event.keyCode : event.which);
                        if(keycode == '13'){
                            doFilter();
                        }
                    });
                });
                $("input[name='filter-button']").on("click",doFilter);
                $("input[name='filter-button-clear']").on("click",function(){
                    $(".filter-field").each(function(){
                        $(this).val("");
                    });
                    doFilter();
                });

                $(".filter-panel-tool").find("a").on("click",function(){
                    if ($(this).hasClass("panel-tool-expand")) {
                        $(this).removeClass("panel-tool-expand");

                        $("#filter-body").slideDown("slow");
                    } else {
                        $(this).addClass("panel-tool-expand");

                        $("#filter-body").slideUp("slow");
                    }
                });
            }

            $(document).ready(function(){
                $("#dg").datagrid(conf);
                initFilter();

                $.extend($.fn.validatebox.defaults.rules, {
                    notEmpty: {
                        validator: function(value){
                            return $.trim(value).length >= 1;
                        },
                        message: '<s:message code="validation.string.not-empty" />'
                    }
                });

                $(".btn-add").on("click", function() {
                    var dialogId = "dlg-add-new";
                    $("body").append("<div id='" + dialogId + "' />");
                    $("#" + dialogId).dialog({
                                  title: '<s:message code="dialog.add-new-record" />',
                                  width: 400,
                                  height: 200,
                                  closed: false,
                                  cache: false,
                                  href: '<c:url value="/newRecord" />',
                                  modal: true,
                                  buttons:[{
                                                text:'<s:message code="dialog.save" />',
                                                handler:function() {
                                                    var form = $("#NameModelForm","#" + dialogId);

                                                    if ($(form).form('validate')) {

                                                        var formData = $(form).serialize();
                                                                        $.ajax({
                                                                            url: '<c:url value="/saveRecord" />',
                                                                            method: 'POST',
                                                                            data: formData
                                                                        }).done(function(response){
                                                                            $('#dg').datagrid('reload');
                                                                            $.messager.show({
                                                                                title:'<s:message code="dialog.notification" />',
                                                                                msg:response,
                                                                                timeout:5000,
                                                                                showType:'slide'
                                                                            });
                                                                            $("#" + dialogId).dialog('close');
                                                                            $("#" + dialogId).dialog('destroy');
                                                                        }).fail(function(jqXHR){
                                                                            $.messager.alert('<s:message code="dialog.error" />',jqXHR.responseText, 'error');
                                                                        });
                                                    }
                                                }
                                           },
                                           {
                                  				text:'<s:message code="dialog.close" />',
                                  				handler:function(){
                                  				    $("#" + dialogId).dialog('close');
                                  				    $("#" + dialogId).dialog('destroy');
                                  				}
                                  		   }],
                                  onLoadError: function(jqXHR) {
                                                      $.messager.alert('<s:message code="dialog.error" />',jqXHR.responseText, 'error');
                                                      $("#" + dialogId).dialog('close');
                                                      $("#" + dialogId).dialog('destroy');
                                                  }
                              });
                });
            });
        </script>
    </head>
    <body>
        <table id="dg" title="<s:message code="datagrid.title" />" style="width:700px;">
        </table>
        <div id="tb" style="padding: 0px;">
            <div id="tb-filter">
                <div id="filter-head" class="panel-header">
                    <div class="panel-title"><s:message code="datagrid.filter.title" /></div>
                    <div class="panel-tool filter-panel-tool">
                        <a href="javascript:void(0)" class="panel-tool-collapse"></a>
                    </div>
                </div>
                <div id="filter-body" style="padding:3px">
                    <fieldset>
                        <span><s:message code="dictName.name" /></span>
                        <input id="name" class="filter-field" type="text" />
                        <input type="button" name="filter-button" value="<s:message code="datagrid.filter.button.title" />" />
                        <input type="button" name="filter-button-clear" value="<s:message code="datagrid.filter.button-clear.title" />" />
                    </fieldset>
                </div>
                <div id="toolbar-buttons">
                    <a href="javascript:void(0)" title="<s:message code="dialog.add-new-record" />" class="easyui-linkbutton btn-add" iconCls="icon-add" plain="true"></a>
                </div>
            </div>
        </div>
    </body>
</html>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="s" %>
<!DOCTYPE html>
<html>
    <head>
        <!-- <link type="text/css" href="<c:url value="/resources/css/easyui/themes/default/easyui.css"/>" rel="stylesheet"/>
        <link type="text/css" href="<c:url value="/resources/css/easyui/themes/icon.css"/>" rel="stylesheet"/>
        <script type="text/javascript" src="<c:url value="/resources/js/jquery-2.1.4.min.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/resources/js/easyui/jquery.easyui.min.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/resources/js/easyui/locale/easyui-lang-ru.js"/>"></script>
        -->
    </head>
    <body>
        <form id="NameModelForm" style="padding: 10px;">
            <div>
                <label for="name"><s:message code="dictName.name" /></label>
                <input class="easyui-validatebox" type="text" name="name" data-options="required:true, validType:'notEmpty'" maxLength="255" />
            </div>
        </form>
    </body>
</html>
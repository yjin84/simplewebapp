SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema test_org
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `test_org` ;

-- -----------------------------------------------------
-- Schema test_org
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `test_org` DEFAULT CHARACTER SET utf8 ;
USE `test_org` ;

-- -----------------------------------------------------
-- Table `test_org`.`org_name`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `test_org`.`org_name` ;

CREATE TABLE IF NOT EXISTS `test_org`.`org_name` (
  `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Идентификатор',
  `name` VARCHAR(255) NOT NULL COMMENT 'Имя',
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = utf8
COMMENT = 'Таблица имен';

SET SQL_MODE = '';
GRANT USAGE ON *.* TO org_test_user;
 DROP USER org_test_user;
SET SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';
CREATE USER 'org_test_user' IDENTIFIED BY 'org_test_user';

GRANT SELECT ON TABLE `test_org`.* TO 'org_test_user';
GRANT SELECT, INSERT, TRIGGER ON TABLE `test_org`.* TO 'org_test_user';

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
